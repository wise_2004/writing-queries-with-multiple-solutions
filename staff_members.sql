-- Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
SELECT
    s.store_id,
    s.first_name || ' ' || s.last_name AS staff_name,
    SUM(p.amount) AS total_revenue
FROM
    staff s
JOIN
    payment p ON s.staff_id = p.staff_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY
    s.store_id, s.staff_id
HAVING
    SUM(p.amount) = (
        SELECT MAX(total_revenue_per_store)
        FROM (
            SELECT
                s.store_id,
                s.staff_id,
                SUM(p.amount) AS total_revenue_per_store
            FROM
                staff s
            JOIN
                payment p ON s.staff_id = p.staff_id
            WHERE
                EXTRACT(YEAR FROM p.payment_date) = 2017
            GROUP BY
                s.store_id, s.staff_id
        ) AS subquery
        WHERE
            subquery.store_id = s.store_id
    )
ORDER BY
    s.store_id;
