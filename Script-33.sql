-- Which five movies were rented more than the others, and what is the expected age of the audience for these movies?

SELECT
    f.title,
    f.rating,
    COUNT(r.rental_id) AS rental_count
FROM
    rental r
INNER JOIN
    inventory i ON r.inventory_id = i.inventory_id
INNER JOIN
    film f ON i.film_id = f.film_id
GROUP BY
    f.film_id
ORDER BY
    rental_count DESC
LIMIT 5;
