SELECT
    a.actor_id,
    a.first_name || ' ' || a.last_name AS actor_name,
    MAX(f.release_year) AS last_film_release_year,
    CURRENT_DATE - DATE(MAX(f.release_year) || '-01-01') AS inactivity_period_days
FROM
    actor a
LEFT JOIN
    film_actor fa ON a.actor_id = fa.actor_id
LEFT JOIN
    film f ON fa.film_id = f.film_id
GROUP BY
    a.actor_id, actor_name
ORDER BY
    inactivity_period_days DESC;
